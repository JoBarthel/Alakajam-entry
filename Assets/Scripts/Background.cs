﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Background : MonoBehaviour {

    SpriteRenderer sprite;

	// Use this for initialization
	void Start () {
        sprite = gameObject.GetComponent<SpriteRenderer>();
	}
	
	// Update is called once per frame
	void Update () {
        Vector2 lowerBound = Camera.main.ViewportToWorldPoint(new Vector2(0, 0));
        Vector2 upperBound = Camera.main.ViewportToWorldPoint(new Vector2(1, 1));
        Vector2 port = upperBound - lowerBound;
        sprite.size = port;
	}
}
