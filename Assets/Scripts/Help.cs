﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Help : MonoBehaviour
{

	public GameObject help1;

	public GameObject help2;

	public GameObject help3;

	public void Hide()
	{
		gameObject.SetActive(false);
	}

	public void Show()
	{
		gameObject.SetActive(true);
		help1.SetActive(true);
		help2.SetActive(false);
		help3.SetActive(false);
	}

	public void ShowHelp2()
	{
		help2.SetActive(true);
		help1.SetActive(false);
		help3.SetActive(false);
	}

	public void ShowHelp3()
	{
		help3.SetActive(true);
		help1.SetActive(false);
		help2.SetActive(false);
	}
	
}
