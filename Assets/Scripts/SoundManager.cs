﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundManager : MonoBehaviour {

    [SerializeField]
    private AudioSource effectsSource;

    private static SoundManager singleton;

    public static SoundManager Singleton
    {
        get {
            return singleton; }
    }

	// Use this for initialization
	void Start () {
        if (singleton == null)
            singleton = this;
    }
	
    public void PlayEffect(AudioClip val)
    {
        effectsSource.clip = val;
        effectsSource.Play();

    }

	// Update is called once per frame
	void Update () {
		
	}
}
