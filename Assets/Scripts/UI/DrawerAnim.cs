﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DrawerAnim : MonoBehaviour
{

	private bool _isOpen;
	private float _drawerOpenPosX;
	private float _drawerClosedPosX;
	
	public RectTransform optionsMenu;
	private static float animationSpeed = 300;
	
	void Start()
	{
		_isOpen = false;
		_drawerClosedPosX = transform.localPosition.x;
	}
	
	
	public void Switch()
	{
		_drawerOpenPosX = _drawerClosedPosX + optionsMenu.rect.width - optionsMenu.GetComponent<HorizontalLayoutGroup>().padding.left;
		if (_isOpen)
			StartCoroutine(Move(_drawerOpenPosX, _drawerClosedPosX));
		
		else
			StartCoroutine(Move(_drawerClosedPosX, _drawerOpenPosX));
		
		_isOpen = !_isOpen;

	}


	private IEnumerator Move(float begin, float end)
	{
		float distance = Mathf.Abs(begin - end);
		float animationTime = distance / animationSpeed; 
		Vector3 startingPos = transform.localPosition;

		float currentTime = 0;
		while (currentTime < animationTime)
		{
			float fracTime = currentTime / animationTime;
			startingPos.x = Mathf.Lerp(begin, end, fracTime);
			transform.localPosition = startingPos;
			yield return null;
			currentTime += Time.deltaTime;
		}

		startingPos.x = end;
		transform.localPosition = startingPos;
	}
}
