﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIButtonLockController : MonoBehaviour
{

	private Button _button;
	
	public TechData requiredTech;
	public TechData researchedTech;
	public Sprite researchDoneSprite;
	
	void Start ()
	{
		_button = GetComponent<Button>();
		_button.interactable = false;
		EventManager<string>.Subscribe("TECH_UNLOCKED", EnableSelf );
		EventManager<string>.Subscribe("TECH_LOCKED", DisableSelf);

		if (researchedTech != null)
		{
			EventManager<string>.Subscribe("TECH_UNLOCKED", ResearchDone);
		}
	}

	public void EnableSelf( string techName)
	{
		if(techName == requiredTech.name)
			_button.interactable = true;
	}

	public void DisableSelf(string techName)
	{
		if (techName == requiredTech.name)
		{
			if(_button != null)
				_button.interactable = false;
		}
	}

	public void ResearchDone(string techName)
	{
		if (techName == researchedTech.name)
		{
			_button.enabled = false;
			_button.targetGraphic.GetComponent<Image>().sprite = researchDoneSprite;
		}
	}
	
}
