﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class NightButton : MonoBehaviour
{
	public Sprite moon;
	public Sprite sun;

	private Image _image;
    private WaveManager wave;

	void Start ()
	{
		_image = gameObject.GetComponent<Image>();
        wave = gameObject.GetComponent<WaveManager>();
		EventManager<int>.Subscribe("WAVE_END", Show);
        EventManager<int>.Subscribe("WAVE_START", Hide);
	}
	

    private void Hide(int dump)
    {
	    _image.sprite = moon;
    }

	public void Show(int nightNumber)
	{
		_image.sprite = sun;
	}

	public void NightBegins()
	{
        wave.LaunchWave();
		EventSystem.current.SetSelectedGameObject(null);
	}
	
	
}
