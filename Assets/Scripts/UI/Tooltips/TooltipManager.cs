﻿using System.Collections;
using System.Collections.Generic;
using System.Net.Mime;
using UnityEngine;
using UnityEngine.Experimental.UIElements;
using UnityEngine.Internal.Experimental.UIElements;
using UnityEngine.UI;

public class TooltipManager : MonoBehaviour
{

    private static TooltipManager _instance;

    public static TooltipManager Instance
    {
        get { return _instance; }
    }
    public Text title;
    public Text description;
    public Text flavor;

    public void Start()
    {
        if (_instance == null)
            _instance = this;
        
        gameObject.SetActive(false);
    }
    
    public void Display(Tooltip tooltip)
    {
        title.text = tooltip.title;
        description.text = tooltip.description;
        if(tooltip.flavorText != "")
            flavor.text = "\""+tooltip.flavorText + "\"";
        gameObject.SetActive(true);
    }

    public void StopDisplay()
    {
        gameObject.SetActive(false);
        title.text = "";
        description.text = "";
        flavor.text = "";
    }

}
