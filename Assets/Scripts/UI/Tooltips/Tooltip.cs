﻿using UnityEngine;

[CreateAssetMenu(fileName = "Tooltip", menuName = "Tooltip", order = 1)]
public class Tooltip : ScriptableObject
{
    public string title;
    public string description;
    public string flavorText;
}
