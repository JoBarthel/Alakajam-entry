﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class TooltipDisplayer : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{
	public Tooltip tooltip;

	public void OnPointerEnter(PointerEventData eventData)
	{
		TooltipManager.Instance.Display(tooltip);
	}

	public void OnPointerExit(PointerEventData eventData)
	{
		TooltipManager.Instance.StopDisplay();
	}
}
