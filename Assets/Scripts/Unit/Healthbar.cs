﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Healthbar : MonoBehaviour
{

	public UnitAi unit;
	public Image greenArea;

	private float _totalBarWidth;
	
	void Start()
	{
		_totalBarWidth = ((RectTransform) transform).rect.width;
	}
	
	void Update ()
	{
		float fracHealth = 1f;
		if (unit.MaxHealth > 0)
			fracHealth = (float) unit.Health / unit.MaxHealth;

		Rect currentRect = greenArea.rectTransform.rect;
		currentRect.width = Mathf.Lerp(0, _totalBarWidth, fracHealth);
		greenArea.rectTransform.sizeDelta = new Vector2(currentRect.width, currentRect.height);



	}
}
