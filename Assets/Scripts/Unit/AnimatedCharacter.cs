﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimatedCharacter : MonoBehaviour {

    private Animator animation_controller;
    private bool rotated = false;
    public Transform parent;
    private UnitData data;
    private float timer;

    // Use this for initialization
    void Start () {
        animation_controller = gameObject.GetComponent<Animator>();
        Debug.Assert(animation_controller != null);
        Debug.Assert(parent != null);
        data = gameObject.GetComponentInParent<UnitDataHandler>().data;
        timer = 0;
    }

    private void Rotate()
    {
        rotated = !rotated;
        Vector3 tmp = new Vector3();
         tmp =   parent.localScale;
        tmp.Scale(new Vector3(-1, 1, 1));
        parent.localScale = tmp;
    }

    private void ResetState()
    {
        animation_controller.SetBool("move", false);
    }

    public void MoveRight()
    {
        ResetState();
        if (rotated)
            Rotate();
        animation_controller.SetBool("move", true);
    }
    
    public void MoveLeft()
    {
        ResetState();
        if (!rotated)
            Rotate();
        animation_controller.SetBool("move", true);
    }

    public void Attack()
    {
            ResetState();
            animation_controller.SetTrigger("attack");
    }

    public void Stop()
    {
        ResetState();

    }

	// Update is called once per frame
	void Update () {
        if (animation_controller.GetBool("move"))
        {
            int direction = rotated ? -1 : 1;
            parent.Translate(new Vector3(direction * data.speed * Time.deltaTime, 0, 0));
        }
     }
}
