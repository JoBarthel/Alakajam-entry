﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UnitAi : MonoBehaviour {

    protected StagePathing pathingCalculator;
    protected UnitPathing pathing;
    protected GameObject target;
    protected UnitData data;
    public AnimatedCharacter animationController;
    public TechData healthTech1;
    public TechData damageTech;
    protected float attackTimer;
    [SerializeField]
    protected GameObject soul;

    [SerializeField]
    protected int damage;

    private int _maxHealth;

    public int MaxHealth
    {
        get { return _maxHealth; }
    }

    [SerializeField]
    protected int health;

    public int Health
    {
        get { return health; }
    }

    // Use this for initialization
	void Start () {

        StartInit();
	}

    protected void StartInit()
    {
        RegisterInStage(); //TEMP
        data = gameObject.GetComponent<UnitDataHandler>().data;
        _maxHealth = data.health;
        health = _maxHealth;
        attackTimer = 0;
        damage = data.damage;
        EventManager<int>.Subscribe("WAVE_END", SetHealthFull);

        if (gameObject.CompareTag("Friend"))
        {
            EventManager<string>.Subscribe("TECH_UNLOCKED", Upgrade);
            EventManager<int>.Subscribe("HEAL", Heal);


            if (healthTech1 != null && TechStatus.TestTech(healthTech1.name))
                Upgrade(healthTech1.name);
            if (damageTech != null && TechStatus.TestTech(damageTech.name))
                Upgrade(damageTech.name);


        }

    }

    protected void Heal(int heal)
    {
        health += heal;
        if (health > MaxHealth)
            health = MaxHealth;

    }

	protected void SetHealthFull(int dump = 0)
    {
        health = MaxHealth;
    }

    public void RegisterInStage()
    {
        if (pathingCalculator != null)
            UnregisterInStage();
        pathingCalculator = gameObject.GetComponentInParent<StagePathing>();
        if (pathingCalculator != null)
            pathingCalculator.Register(gameObject);
        pathing = gameObject.GetComponent<UnitPathing>();
    }

    public void UnregisterInStage()
    {
        pathingCalculator.Unregister(gameObject);
    }

	// Update is called once per frame
	void Update () {
		if (pathingCalculator != null)
        {
            target = pathingCalculator.GetClosest(gameObject);
            if (!pathing.playerControlled && target != null)
            {
                if (Mathf.Abs(transform.position.x - target.transform.position.x) > data.range)
                    pathing.Go(target.transform.position.x);
                else
                {
                    attackTimer -= Time.deltaTime;
                    if (attackTimer <= 0)
                    {
                        animationController.Attack();
                        DealDamage();
                        attackTimer = data.attackSpeed;
                    }

                }
            }
        }
	}

    public void TakeDamage(int n)
    {
        health -= n;
        if (health <= 0)
        {
            if (gameObject.CompareTag("Demon"))
            {
                GameObject newSoul;
                newSoul = GameObject.Instantiate(soul);
                soul.transform.position = this.transform.position;
                soul.GetComponent<SoulRelease>().Init(data.bounty);
                
            }
            Destroy(gameObject);
        }
    }

    public virtual void DealDamage()
    {
        if (target != null)
            target.GetComponent<UnitAi>().TakeDamage(damage);
    }

    public void Upgrade(string techName)
    {
        if (techName == healthTech1.name)
        {
            _maxHealth += healthTech1.bonus;
            health += healthTech1.bonus;
        }
        if (techName == damageTech.name)
        {
            damage += damageTech.bonus;
        }
    }

    private void OnDestroy()
    {
        if (pathingCalculator != null)
            pathingCalculator.Unregister(gameObject);
        EventManager<int>.Unsubscribe("WAVE_END", SetHealthFull);

        if (gameObject.CompareTag("Friend"))
        {
            EventManager<string>.Unsubscribe("TECH_UNLOCKED", Upgrade);
            EventManager<int>.Unsubscribe("HEAL", Heal);
        }
    }
   
}
