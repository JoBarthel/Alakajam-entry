﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "UnitData", menuName = "Stats/Unit", order = 1)]
public class UnitData : ScriptableObject {
    public float speed;
    public int damage;
    public float range;
    public int health;
    public int bounty;
    public string requiredBuilding;
    public int cost;
    public float attackSpeed;
}
