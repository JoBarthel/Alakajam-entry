﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Elevator : MonoBehaviour {

    [SerializeField]
    protected int levels = 0;

    protected Plant plant;
    


    public int MoveToLevel(GameObject toMove,int currentLevel,int destination)
    {
        Debug.Log(currentLevel + " " + destination);
        toMove.transform.Translate(new Vector3(0,(destination-currentLevel)*plant.stageHeight));
        toMove.transform.SetParent(plant.stages[destination].transform);
        return destination;
    }

	// Use this for initialization
	void Start () {
        plant = gameObject.GetComponent<Plant>();
	}
	
	// Update is called once per frame
	void Update () {
		
	}
    
    public int FloatToStage(float posY)
    {
        int tmp;
        tmp = (int)((posY+27.0f) / (plant.stageHeight));
        if (tmp > plant.stages.Count - 1)
            tmp = plant.stages.Count - 1;
        if (tmp < 0)
            tmp = 0;
        return tmp;
    }

}
