﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoulRelease : MonoBehaviour {

    protected Transform target;

    [SerializeField]
    protected float speed;

    protected int value = 10;

	// Use this for initialization
	void Start () {
        target = GameObject.FindGameObjectWithTag("Soulcounter").transform;
	}
	

    public void Init(int val)
    {
        value = val;
    }

	// Update is called once per frame
	void Update () {
        Vector2 speedVect = new Vector2();
        speedVect = target.position - transform.position;
        if (speedVect.magnitude >= 0.1f && (speedVect.x > 0 || speedVect.y > 0))
        {
            speedVect.Normalize();
            speedVect *= speed;
            transform.Translate(speedVect);
        }
        else
        {
            EventManager<int>.Raise("UNIT_DEATH", value);
            GameObject.Destroy(gameObject);
        }
        
    }
}
