﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UnitController : MonoBehaviour {

    private bool isSelected;
    // hack sale pour qu'il annule pas la premiere selection
    private bool justSelected;
    private UnitPathing pathing;
    public AudioClip selectionSound;
    protected Elevator elevator;

    [SerializeField]
    private GameObject selectionIndicator;

    public void Select()
    {
        isSelected = true;
        justSelected = true;
        selectionIndicator.SetActive(true);
    }

    public void DeSelect()
    {
        if (!justSelected)
        {
            isSelected = false;
            selectionIndicator.SetActive(false);
        }
        else
            justSelected = false;
    }
    

    // Use this for initialization
    void Start () {
        pathing = gameObject.GetComponent<UnitPathing>();
        elevator = GetComponentInParent<Elevator>();
    }
	
	// Update is called once per frame
	void Update () {
        if (isSelected && Input.GetButtonUp("Fire2"))
        {
            int temp;
            Vector3 mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            temp = elevator.FloatToStage(mousePos.y) - pathing.currentStage;
            //move to the correct point
            pathing.Go(mousePos.x,temp);
            pathing.playerControlled = true;
        }
        else if (isSelected && Input.GetButtonUp("Fire1"))
        {
            DeSelect();
        }
	}

    void OnMouseUp()
    {
        Select();
    }
}
    