﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UnitPathing : MonoBehaviour {

    private float xDest = 0;
    private float xDestFinal;
    private int yDest = 0;
    public AnimatedCharacter anim;
    private bool moving = false;
    public float margin = 0.1f;

    [SerializeField]
    protected float plantWith = 50f;

    public bool playerControlled;
    public int currentStage = 0;

    protected Elevator elevator;

    void Start () {
        elevator = GetComponentInParent<Elevator>();
        currentStage = elevator.FloatToStage(transform.position.y);
    }

    public void Go(float dest,int desty = 0)
    {
        if (desty != 0)
        {
            yDest = desty;
            xDestFinal = dest;
            Go(0);
            return;
        }

        xDest = dest;
        if (xDest >= plantWith) xDest = plantWith;
        if (xDest <= -plantWith) xDest = -plantWith;
        if (xDest < transform.position.x - margin)
        {
            anim.MoveLeft();
        }
        else if (xDest > transform.position.x + margin)
        {
            anim.MoveRight();
        }
        moving = true;
    }



    // Update is called once per frame
    void Update()
    {
        if (moving && xDest <= transform.position.x + margin && xDest >= transform.position.x - margin)
        {
            if (yDest != 0)
            {
                UnitAi ai = gameObject.GetComponent<UnitAi>();
                currentStage = elevator.MoveToLevel(gameObject,currentStage, currentStage+yDest);
                yDest = 0;
                Go(xDestFinal+currentStage);
                ai.RegisterInStage();
            }
            else
            {
                moving = false;
                anim.Stop();
                playerControlled = false;
            }
        }
    }
}
