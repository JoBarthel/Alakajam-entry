﻿
using System.Security.Cryptography.X509Certificates;
using UnityEngine;

public class Building : MonoBehaviour
{
    public BuildingData data;

    public void Start()
    {
        TechStatus.Unlock(data.techUnlocked);
    }

    protected void OnDestroy()
    {
        TechStatus.Lock(data.techUnlocked);
    }
}
