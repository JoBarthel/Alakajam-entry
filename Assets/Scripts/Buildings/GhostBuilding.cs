﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using Object = System.Object;

public class GhostBuilding : MonoBehaviour
{

	private Camera _camera;
	
	public Building actualBuilding;
	public float buildingDistance;
	private List<Slot> _availableSlots;
	public SoulsCounter soulsCounter;	
	private Slot _previousNearestSlot;

	void Awake()
	{
		_availableSlots = new List<Slot>();
		_camera = Camera.main;
		EventManager<Slot>.Subscribe("slotAvailable", AddSlot);
		EventManager<Slot>.Subscribe("slotTaken", RemoveSlot );
		EventManager<Vector3>.Subscribe("constructionEnds", Hide);
		gameObject.SetActive(false);
	}

	void Update ()
	{
		Vector3 pos = _camera.ScreenToWorldPoint(Input.mousePosition);
		pos.z = 0;
		transform.localPosition = pos ;
		
		Slot nearestSlot = UpdateSlots();
		if (nearestSlot != null)
		{
			if (Input.GetMouseButtonUp(0))
			{
				
				TryBuild(nearestSlot);
			}
		}

	}

	public void AddSlot(Slot slot)
	{
		if(slot.Available)
			_availableSlots.Add(slot);
	}

	public void RemoveSlot(Slot slot)
	{
		if (!slot.Available)
			_availableSlots.Remove(slot);
	}

	public void Hide(Vector3 mousePos)
	{
		gameObject.SetActive(false);
	}

	#region Implementation

	private void TryBuild(Slot slot)
	{
		if(soulsCounter.TryPay(actualBuilding.data.cost))
		{
			slot.BuildHere(actualBuilding);
		}
		else
		{
			EventManager<Vector3>.Raise("constructionEnds", Vector3.zero);
		}
	}
	
	private Slot UpdateSlots()
	{
		List<Slot> closeSlots = SlotsInBuildingDistance(this, _availableSlots);
		Slot nearestSlot = NearestSlot(this, closeSlots);

		if(_previousNearestSlot != null)
			_previousNearestSlot.StopShine();
		if (nearestSlot != null)
			nearestSlot.Shine();

		_previousNearestSlot = nearestSlot;
		

		return nearestSlot;
	}
	
	private List<Slot> SlotsInBuildingDistance(GhostBuilding ghostBuilding, List<Slot> slots)
	{
		List<Slot> closeSlots = new List<Slot>();
		foreach (Slot slot in slots)
		{
			if (SlotToBuildingDistance(slot, ghostBuilding) < buildingDistance)
			{
				closeSlots.Add(slot);
			}
		}

		return closeSlots;
	}
	
	private Slot NearestSlot(GhostBuilding ghostBuilding, List<Slot> slots)
	{
		Slot nearestSlot = null;
		float minDistance = float.MaxValue;

		foreach (Slot slot in slots)
		{
			float currentDistance = SlotToBuildingDistance(slot, ghostBuilding);
			if ( currentDistance < minDistance)
			{
				minDistance = currentDistance;
				nearestSlot = slot;
			}
		}

		return nearestSlot;

	}

	private float SlotToBuildingDistance(Slot slot, GhostBuilding gb)
	{
		return (slot.transform.position - gb.transform.position).magnitude;
	}
	
	#endregion
}
