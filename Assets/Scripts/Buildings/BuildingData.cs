﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "BuildingData", menuName = "Stats/Building", order = 1)]
public class BuildingData : ScriptableObject
{

    public int cost;
    public string techUnlocked;
    public string requiredTech;
}
