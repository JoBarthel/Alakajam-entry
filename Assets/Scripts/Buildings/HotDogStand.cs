﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HotDogStand : Building {
    
    [SerializeField]
    protected float cooldown;
    [SerializeField]
    protected int heal;

    protected float timer;

    new protected void Start()
    {
        base.Start();
        timer = cooldown;
    }
    

    // Update is called once per frame
    void Update()
    {
        timer -= Time.deltaTime;
        if (timer <= 0)
        {
            EventManager<int>.Raise("HEAL", heal);
            timer = cooldown;
        }
    }
}
