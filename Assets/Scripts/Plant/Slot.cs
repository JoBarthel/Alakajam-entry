﻿using UnityEngine;

public class Slot : MonoBehaviour
{
    public const float OFFSET_BUILDING = 5f;
    public Building Occupant { get; private set; }
    public bool Available
    {
        get { return available;}
    }

    public Sprite placeholder;
    public SpriteRenderer shine;
    
    private SpriteRenderer _spriteRendererenderer;
    protected bool available;

    private void Start()
    {
        shine.enabled = false;
        _spriteRendererenderer = GetComponent<SpriteRenderer>();
        MakeAvailable();
        Hide(Vector3.zero);
    }
    
    public void Show(GameObject gameObject)
    {
        _spriteRendererenderer.sprite = placeholder;
    }

    public void Hide(Vector3 mousePos)
    {
        _spriteRendererenderer.sprite = null;
        StopShine();
    }

    public void Shine()
    {
        shine.enabled = true;
    }

    public void StopShine()
    {
        shine.enabled = false;
    }

    public void BuildHere(Building building)
    {
        Occupant = Instantiate(building.gameObject, gameObject.transform.parent).GetComponent<Building>();
	
        Vector3 occupantPosition = transform.localPosition;
        occupantPosition.y += OFFSET_BUILDING;
        Occupant.transform.localPosition = occupantPosition;
        
        MakeUnavailable();
        EventManager<Vector3>.Raise("constructionEnds", Input.mousePosition);
    }

    #region Implementation

    private void MakeAvailable()
    {
        Occupant = null;
        available = true;
        EventManager<Slot>.Raise("slotAvailable", this);
        EventManager<GameObject>.Subscribe("constructionBegins", Show);
        EventManager<Vector3>.Subscribe("constructionEnds", Hide);
    }

    private void MakeUnavailable()
    {
        available = false;
        EventManager<Slot>.Raise("slotTaken", this);
        Hide(Vector3.zero);
        EventManager<GameObject>.Unsubscribe("constructionBegins", Show);
        EventManager<Vector3>.Unsubscribe("constructionEnds", Hide);
    }

    private void OnDestroy()
    {
        MakeUnavailable();
    }

    #endregion
}
