﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


//Classe pour savoir qui est sur un stage et quel type
public class StagePathing : MonoBehaviour {
    
    protected class IntelligentGameObject
    {
        public GameObject me;
        public GameObject prev = null;
        public GameObject closest = null;

        public void SetNext(GameObject next)
        {
            if (next == null)
            {
                closest = prev;
                return;
            }
            if (prev == null)
            {
                closest = next;
                return;
            }
            float distPrev = Mathf.Abs(me.transform.position.x - prev.transform.position.x);
            float distNext = Mathf.Abs(me.transform.position.x - next.transform.position.x);
            if (distPrev < distNext)
                closest = prev;
            else
                closest = next;
        }
    }

    protected Dictionary<int, IntelligentGameObject> adjacency;

    protected List<IntelligentGameObject> adjacencyCalc;

    protected bool waveHappeningHere;

    protected void SortAll()
    {
        int i = 0;
        int j = 0;
        bool friendFoe = true; //true if we were looking at friends before false if demons
        bool currentFF = true; // current guy
        int friendCount = 0, foeCount = 0;
        IntelligentGameObject prev_friend = null;
        IntelligentGameObject prev_foe = null;
        adjacencyCalc.Sort((x, y) => x.me.transform.position.x.CompareTo(y.me.transform.position.x)); // sort by x position
        for (j = 0; j < adjacencyCalc.Count; j++)
        {
            currentFF = adjacencyCalc[j].me.CompareTag("Friend");
            if (j == 0)
                friendFoe = currentFF;
            if (currentFF) //update last friend
            {
                friendCount++;
                if (prev_foe != null)
                    adjacencyCalc[j].prev = prev_foe.me;
                prev_friend = adjacencyCalc[j];
            }
            else
            {
                foeCount++;
                if (prev_friend != null)
                    adjacencyCalc[j].prev = prev_friend.me;
                prev_foe = adjacencyCalc[j];
            }
            if (currentFF != friendFoe)
            {
                while( i < j )
                {
                    adjacencyCalc[i].SetNext(adjacencyCalc[j].me);
                    i++;
                }
            }
            friendFoe = currentFF;
        }
        while (i < j)
        {
            adjacencyCalc[i].SetNext(null);
            i++;
        }
        if (foeCount == 0 && waveHappeningHere)
        {
            EventManager<int>.Raise("NO_FOES_ROOT", 0);
        }
        else if(friendCount == 0 && waveHappeningHere)
        {
            EventManager<int>.Raise("NO_FRIENDS_ROOT", 0);
        }
    }

    private void Awake()
    {
        adjacency = new Dictionary<int, IntelligentGameObject>();
        adjacencyCalc = new List<IntelligentGameObject>();
    }

    private void StopWave(int dump)
    {
        waveHappeningHere = false;
    }

    // Use this for initialization
    void Start () {
        EventManager<int>.Subscribe("WAVE_END", StopWave);

    }

    private void OnDestroy()
    {
        EventManager<int>.Unsubscribe("WAVE_END", StopWave);
        foreach(IntelligentGameObject i in adjacencyCalc)
        {
            Object.Destroy(i.me);
        }
    }

    // Update is called once per frame
    void Update () {
        SortAll();	
	}

    
    
    //method to add a object
    public void Register(GameObject toAdd)
    {
        IntelligentGameObject newElt = new IntelligentGameObject();
        newElt.me = toAdd;
        adjacency[toAdd.GetInstanceID()] = newElt;
        adjacencyCalc.Add(newElt);
        if (!toAdd.CompareTag("Friend"))
            waveHappeningHere = true;
    }
    

    public void Unregister(GameObject toRemove)
    {
        IntelligentGameObject tmp;
        int id = toRemove.GetInstanceID();
        if (adjacency.ContainsKey(id))
        {
            tmp = adjacency[id];
            adjacency.Remove(id);
            adjacencyCalc.Remove(tmp);
        }
    }
    
    public GameObject GetClosest(GameObject caller)
    {
        if (adjacency.ContainsKey(caller.GetInstanceID()))
            return adjacency[caller.GetInstanceID()].closest;
        else
            return null;
    }


   
}
