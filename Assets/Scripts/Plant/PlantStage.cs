﻿using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.InteropServices;
using UnityEditor;
using UnityEngine;
using UnityEngine.Experimental.PlayerLoop;

public class PlantStage : BeanObject
{
	public GameObject slotPrefab;
	public PlantStageForm currentForm;
	public GameObject middleFormPrefab;
	public float stageFloor = 12.5f;
	public float spaceBetweenTwoSlot = 25f;
	public int numberOfSlotsOnEachSide = 2;

	private void Awake()
	{
		Vector3 currentPosRight = new Vector3(spaceBetweenTwoSlot, stageFloor, 0);
		Vector3 currentPosLeft = new Vector3(-spaceBetweenTwoSlot, stageFloor, 0);
		
		for(int i = 0 ; i < numberOfSlotsOnEachSide ; i++)
		{
			GameObject leftSlotInstance = Instantiate(slotPrefab, transform);
			leftSlotInstance.gameObject.transform.localPosition = currentPosLeft;
			
			GameObject rightSlotInstance = Instantiate(slotPrefab, transform);
			rightSlotInstance.gameObject.transform.localPosition = currentPosRight;

			currentPosRight.x += spaceBetweenTwoSlot;
			currentPosLeft.x -= spaceBetweenTwoSlot;
		}
	}

	public void Grow()
	{
		if (currentForm is Top)
		{
			Destroy(currentForm.gameObject);
			currentForm = Instantiate(middleFormPrefab, transform).GetComponent<PlantStageForm>();
			currentForm.gameObject.transform.SetPositionAndRotation( transform.position, Quaternion.identity);
			GainNewSlots();
		}
	}

	private void GainNewSlots()
	{
		numberOfSlotsOnEachSide += 1;
		GameObject leftSlotInstance = Instantiate(slotPrefab, transform);
		leftSlotInstance.gameObject.transform.localPosition = new Vector3( numberOfSlotsOnEachSide * spaceBetweenTwoSlot, stageFloor, 0 );
		GameObject rightSlotInstance = Instantiate(slotPrefab, transform);
		rightSlotInstance.gameObject.transform.localPosition = new Vector3(-numberOfSlotsOnEachSide * spaceBetweenTwoSlot, stageFloor, 0);
	}

}
