﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class Plant : BeanObject
{
	public GameObject topPrefab;
	public List<PlantStage> stages;
	public float stageHeight;
	public int growthCost;
	public SoulsCounter soulsCounter;
    public int lowestLevel = 0;
    public int heightToWin = 12;
    public int currentHeight = 2;

	#region Methods
	
	public void Grow()
	{
		if (soulsCounter.TryPay(growthCost))
		{
			int lastStageIndex = stages.Count - 1;
			PlantStage lastStage = stages[lastStageIndex];
			lastStage.Grow();

			GameObject stageInstance = Instantiate(topPrefab, gameObject.transform);
			stageInstance.transform.localPosition = NewStagePosition(lastStage);
			stages.Add(stageInstance.GetComponent<PlantStage>());
            growthCost += 10;

			RescaleCamera();
            currentHeight++;
		}
        if (currentHeight == heightToWin)
            gameObject.GetComponent<SceneSwitcher>().Load("Win");

	}

    public void Invasion()
    {
        Destroy(stages[0].gameObject);
        stages.RemoveAt(0);
        if (stages.Count == 0)
            gameObject.GetComponent<SceneSwitcher>().Load("Loose");
        RescaleCameraDown();
        lowestLevel++;
    }

    private void RescaleCameraDown()
    {
        Camera.main.orthographicSize -= stageHeight / 2;
        Vector3 newPos = Camera.main.transform.position;
        newPos.y += stageHeight / 2;
        Camera.main.transform.SetPositionAndRotation(newPos, Quaternion.identity);
    }

    private void RescaleCamera()
	{
		Camera.main.orthographicSize += stageHeight / 2;
		Vector3 newPos = Camera.main.transform.position;
		newPos.y += stageHeight /2;
		Camera.main.transform.SetPositionAndRotation(newPos, Quaternion.identity);
	}
	#endregion
	
	private Vector2 NewStagePosition(PlantStage previousStage)
	{
		return new Vector2(0, previousStage.transform.localPosition.y + (stageHeight));
	}
	
	
}
