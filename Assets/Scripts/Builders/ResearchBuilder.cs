﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ResearchBuilder: MonoBehaviour
{

	public TechData CosplayWeapons;
	public TechData recruitAChef;
	public SoulsCounter soulsCounter;

	public void ResearchChef()
	{
		ResearchTech(recruitAChef);
	}

	public void ResearchWeapons()
	{
		ResearchTech(CosplayWeapons);
	}
	private void ResearchTech(TechData tech)
	{
		if( soulsCounter.TryPay(tech.cost))
			TechStatus.Unlock(tech.name);
	}
}
