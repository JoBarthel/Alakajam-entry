﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using Object = System.Object;

public class UnitBuilder : MonoBehaviour
{

	public float unitZPosition = -3f;
	public UnitDataHandler vikingPrefab;
	public UnitDataHandler gunnerPrefab;
	public UnitDataHandler otakuPrefab;
	
	public SoulsCounter soulsCounter;
	
	public void CreateViking()
	{
		TryBuyUnit(vikingPrefab);
	}

	public void CreateGunner()
	{
		TryBuyUnit(gunnerPrefab);
	}

	public void CreateOtaku()
	{
		TryBuyUnit(otakuPrefab);
	}
	
	private void TryBuyUnit(UnitDataHandler unit)
	{
		if (soulsCounter.TryPay(unit.data.cost))
		{
			CreateUnit(unit);
		}
	}
	
	private GameObject CreateUnit(UnitDataHandler unitPrefab)
	{
		
		Type typeOfCreationBuilding = Type.GetType(unitPrefab.data.requiredBuilding + ", Assembly-CSharp");

		if (typeOfCreationBuilding == null)
		{
			Debug.LogError("Creation Building Type for this unit could not be found. Did you fill the requiredBuilding field ?");
			return new GameObject();
		}


		Building buildingInstance = FindObjectOfType(typeOfCreationBuilding) as Building;
		if (buildingInstance == null)
		{
			Debug.LogError("Could not find creation building for this unit in scene");
			return new GameObject();
		}

		Transform creationBuilding = buildingInstance.transform;
		StagePathing stageParent = creationBuilding.GetComponentInParent<StagePathing>();
		
		if (!stageParent)
			Debug.Log("Could not find suitable parent for this unit. Is the creation building the child of an object with the stagepathing component ?");


		Transform parent = stageParent.transform;
		GameObject unitInstance = Instantiate(unitPrefab.gameObject, parent);

		Vector3 startingPos = creationBuilding.position;
		startingPos.z = unitZPosition;
		unitInstance.transform.SetPositionAndRotation(startingPos, Quaternion.identity);

		return unitInstance;
	}
}
