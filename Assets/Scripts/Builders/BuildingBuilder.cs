﻿
using UnityEngine;

public class BuildingBuilder : MonoBehaviour
{
	public GhostBuilding ghostTavoon;
	public GhostBuilding ghostHotDog;
	public GhostBuilding ghostConvention;
	
	void Update () {

		if (Input.GetMouseButtonUp(1))
		{
			HandleRightClick();
		}
		
	}
	
	public void CreateTavoon()
	{
		CreateGhostBuilding(ghostTavoon);
	}

	public void CreateHotDogStand()
	{
		CreateGhostBuilding(ghostHotDog);
	}

	public void CreateConventionCenter()
	{
		CreateGhostBuilding(ghostConvention);
	}
	
	public void CreateGhostBuilding(GhostBuilding ghostPrefab)
	{
		EventManager<GameObject>.Raise("constructionBegins", ghostPrefab.gameObject);
		ghostPrefab.gameObject.SetActive(true);
	}

	#region Implementation

	private void HandleRightClick()
	{
		if (ghostTavoon.isActiveAndEnabled)
		{
			EventManager<Vector3>.Raise("constructionEnds", Input.mousePosition);
		}
	}

	#endregion
	
}
