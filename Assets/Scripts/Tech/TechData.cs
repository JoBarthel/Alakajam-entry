﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "TechData", menuName = "Stats/Tech", order = 1)]
public class TechData : ScriptableObject
{
    public string name;
    public int cost;
    public int bonus;
}
