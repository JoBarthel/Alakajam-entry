﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TechStatus  {

    static Dictionary<string, int> techCounter = new Dictionary<string, int>();

    public static void Unlock(string name)
    {
        if (techCounter.ContainsKey(name))
            techCounter[name]++;
        else
            techCounter[name] = 1;
        if (techCounter[name] == 1)
            EventManager<string>.Raise("TECH_UNLOCKED", name);
    }
    
    //surtout pour les batiments
    public static void Lock(string name)
    {
        if (techCounter.ContainsKey(name))
        { techCounter[name]--;
            if (techCounter[name] == 0)
                EventManager<string>.Raise("TECH_LOCKED", name);
        }
    }
    
    public static bool TestTech(string name)
    {
        return techCounter.ContainsKey(name) && techCounter[name] >= 0;
    }



}
