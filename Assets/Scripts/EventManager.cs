﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using System;
public class EventManager<T>
{
    private static Dictionary<string, List<Action<T>>> events;
    
    public static void Raise(string eventName, T eventArg)
    {
        Init();
        
        if(!events.ContainsKey(eventName))
            return;
        
        foreach (Action<T> action in events[eventName].ToList())
        {
            action(eventArg);
        }
    }

    public static void Subscribe(string eventName, Action<T> action)
    {
        Init(eventName);
        List<Action<T>> actions = new List<Action<T>>();
        events[eventName].Add(action);
    }

    public static void Unsubscribe(string eventName, Action<T> action)
    {
        Init();
        if (events.ContainsKey(eventName))
            events[eventName].Remove(action);
    }

    #region Implementation

    private static void Init(string eventName)
    {
        Init();
        if(!events.ContainsKey(eventName))
            events.Add(eventName, new List<Action<T>>());
    }

    private static void Init()
    {
                
        if(events == null)
            events = new Dictionary<string, List<Action<T>>>();
    }

    #endregion
}
