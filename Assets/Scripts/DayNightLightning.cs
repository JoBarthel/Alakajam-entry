﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DayNightLightning : MonoBehaviour
{
	public float animationDuration;
	public float alphaMin;
	public float alphaMax;


	private SpriteRenderer _spriteRenderer;

	void Start()
	{
		_spriteRenderer = GetComponent<SpriteRenderer>();
		EventManager<int>.Subscribe("WAVE_START", StartDarken);
		EventManager<int>.Subscribe("WAVE_END", StartLighten);
	}

	void Update()
	{
		Vector2 lowerBound = Camera.main.ViewportToWorldPoint(new Vector2(0, 0));
		Vector2 upperBound = Camera.main.ViewportToWorldPoint(new Vector2(1, 1));
		Vector2 port = upperBound - lowerBound;
		_spriteRenderer.size = port;
	}

	public void StartDarken(int nightNumber)
	{
		StartCoroutine(ChangeLightning(alphaMin, alphaMax));
	}

	public void StartLighten(int nightNumber)
	{
		StartCoroutine(ChangeLightning(alphaMax, alphaMin));
	}
	
	private IEnumerator ChangeLightning(float alphaMin, float alphaMax)
	{
		float currentTime = 0;
		while (currentTime < animationDuration)
		{

			float fracTime = currentTime / animationDuration;
			Color currentColor = _spriteRenderer.color;
			currentColor.a = Mathf.Lerp(alphaMin, alphaMax, fracTime);
			_spriteRenderer.color = currentColor;
			currentTime += Time.deltaTime;
			yield return null;
		}
	}


}
