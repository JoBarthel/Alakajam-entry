﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SoulsCounter : MonoBehaviour {

    [SerializeField]
	protected int bank = 0;

    protected Text text;

    public int Bank
    {
        get { return bank; }
    }

    public void AddMoney(int money)
    {
        bank += money;
        text.text = bank.ToString();
    }

    private void Start()
    {
        text = gameObject.GetComponent<Text>();
        text.text = bank.ToString();
        EventManager<int>.Subscribe("UNIT_DEATH", AddMoney);
    }

    //essaye de payer, retourne false si on peut pas
    public bool TryPay(int cost)
    {
        if (cost > bank)
            return false;
        else
        {
            bank -= cost;
            text.text = bank.ToString();
            return true;
        }
    }

    private void OnDestroy()
    {
        EventManager<int>.Unsubscribe("UNIT_DEATH", AddMoney);
    }
}
