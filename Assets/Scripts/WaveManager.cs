﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WaveManager : MonoBehaviour {

    protected class monsterTimer
    {
        private float timer;
        private int monstersLeft;

        public bool IsDone(float timeLeft)
        {
            bool ret;
            timer -= Time.deltaTime;
            ret = timer <= 0 && monstersLeft != 0;
            if (ret)
            {
                float maxTime = (timeLeft / (float)monstersLeft); //maximum time we have to spawn the next monster
                timer = Random.Range(0, maxTime);
                monstersLeft--;
            }
            return ret;
        }

        public void Init(int WaveSpawn, float timeLeft)
        {
            monstersLeft = WaveSpawn;
            if (monstersLeft != 0)
            {
                float maxTime = (timeLeft / (float)monstersLeft); //maximum time we have to spawn the next monster
                timer = Random.Range(0, maxTime);
            }
        }
    }

    protected int wave;
    protected int levelsDestroyed;

    [SerializeField]
    protected Plant plant;

    [SerializeField]
    protected List<GameObject> monsters;

    protected List<monsterTimer> spawnTimers;

    protected bool waveInProgress;

    [SerializeField]
    protected float waveDuration;
    protected float waveTimeLeft;

    public bool WaveInProgress
    {
     
        get { return waveInProgress; }
    }


    protected void DestroyPunyHumans(int humans_left)
    {
        Debug.Log("human loose");
        if (WaveInProgress)
        {
            plant.Invasion();
            EventManager<int>.Raise("WAVE_END", wave);
            waveInProgress = false;
        }
    }

    protected void WeLost(int monsters_left)
    {

        Debug.Log("human win");
        if (waveTimeLeft <= 0 && WaveInProgress)
        {
            EventManager<int>.Raise("WAVE_END", wave);
            waveInProgress = false;
        }
    }


    // Use this for initialization
    void Start () {
        wave = 0;
        spawnTimers = new List<monsterTimer>();
        foreach (GameObject monster in monsters)
        {
            spawnTimers.Add(new monsterTimer());
        }
        EventManager<int>.Subscribe("NO_FRIENDS_ROOT", DestroyPunyHumans);
        EventManager<int>.Subscribe("NO_FOES_ROOT", WeLost);
    }
   

    public void LaunchWave()
    {
        if (!waveInProgress)
        {
            wave++;
            EventManager<int>.Raise("WAVE_START", wave);
            waveInProgress = true;
            int trueWave = wave - 2 * levelsDestroyed;
            if (trueWave < 1) trueWave = 1;
            for (int i = 0; i < spawnTimers.Count; i++)
            {
                int maxMonster, minMonster;
                maxMonster = 5*(trueWave + 1 - (2 * i));
                if (maxMonster < 0) maxMonster = 0;
                minMonster = maxMonster - 5;
                if (minMonster < 0) minMonster = 0;
                spawnTimers[i].Init(Random.Range(minMonster, maxMonster + 1), waveDuration);
            }
            waveTimeLeft = waveDuration;
        }
    }

	// Update is called once per frame
	void Update () {
        if (waveInProgress)
        {
            for (int i = 0; i < spawnTimers.Count; i++)
            {
                if (spawnTimers[i].IsDone(waveTimeLeft))
                {
                    GameObject newMonster;
                    newMonster = GameObject.Instantiate(monsters[i], plant.stages[0].transform);

                }
            }
            waveTimeLeft -= Time.deltaTime;
        }
    }
}
